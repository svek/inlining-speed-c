# This is an example Makefile for a standalone ToriID code

CC=g++

COMPILER_CFLAGS=-std=c++11

optimized:
	make clean
	${CC} -c -O3 ${COMPILER_CFLAGS} *.cpp
	${CC} -o optimized -O3 ${COMPILER_CFLAGS} *.o
	./optimized | tee resultst.txt
	rm optimized

clean:
	rm -f *.o

