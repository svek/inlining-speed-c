#include "base.h"

class FunctionMethod : public A {
public:
	int b;
	int method(const int& i);
};

class InlineFunctionMethod : public A {
public:
	int b;
	inline int method(const int& i) {
		return i+b;
	}
};


class VirtualMethod : public purelyVirtualA {
public:
	int b;
	int method(const int& i) override;
};

class InlineVirtualMethod : public purelyVirtualA {
public:
	int b;
	inline int method(const int& i) override {
		return i+b;
	}
};



extern int glob_b;
extern int glob_bi;
int function(const int& i);


inline int inlinedFunction(const int& i) {
	return glob_bi+i;
}