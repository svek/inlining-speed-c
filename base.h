#ifndef __A
#define __A

class A {
public:
	int method(const int& i);
};

class purelyVirtualA {
public:
	virtual int method(const int& i) = 0;
};

#endif // __A