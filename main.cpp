
#include "base.h"
#include "function.h"

#include <stdio.h>

// Posix/Linux time measurement, plain C
#include <time.h>
#include <sys/time.h>

double get_wall_time(){
	// gives back epoche in us resolution
	struct timeval time;
	if (gettimeofday(&time,NULL)){
		//  Handle error
		return 0;
	}
	return (double)time.tv_sec + (double)time.tv_usec * .000001;
}
double get_cpu_time(){
	return (double)clock() / CLOCKS_PER_SEC;
}

// Time measurement, C++ chrono, C++11
#include <chrono>


#define MEASURE_POSIX(func, text) {\
	double start_time = get_wall_time(); \
	for(int i=0; i<maxi; i++) { func; } \
	printf("  * %e sec : %s\n", get_wall_time() - start_time, text); }
	
#define MEASURE_CHRONO(func, text) {\
	using namespace std::chrono; \
	auto start = std::chrono::high_resolution_clock::now(); \
	for(int i=0; i<maxi; i++) { func; } \
	auto elapsed = std::chrono::high_resolution_clock::now() - start; \
	long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count(); \
	printf("  * %e sec : %s\n", ((double)microseconds)/1e6, text); }
		
	

int main() {
	const int maxi = 1e8;
	
	// the objects we use
	FunctionMethod f;
	VirtualMethod v;
	InlineFunctionMethod fi;
	InlineVirtualMethod vi;
	
	// set some values
	glob_b = 2; glob_bi = 2;
	f.b = 2; fi.b = 2;
	v.b = 2; vi.b = 2;
	
	printf("Time for %.1e iterations of:\n", (double)maxi);

	MEASURE_CHRONO(glob_bi = inlinedFunction(i), "Inlined Real Function");
	MEASURE_CHRONO(fi.b = fi.method(i), "Inlined Real Method");
	MEASURE_CHRONO(vi.b = vi.method(i), "Inlined Virtual Method");
	MEASURE_CHRONO(glob_b = function(i), "Real Function");
	MEASURE_CHRONO(f.b = f.method(i), "Real Method");
	MEASURE_CHRONO(v.b = v.method(i), "Virtual Method");
	
	printf("\n\nJust to avoid optimization of operation:\n");
	printf("f.b=%d fi.b=%d ", f.b, fi.b);
	printf("v.b=%d vi.b=%d ", v.b,vi.b);
	printf("gb=%d gbi=%d ", glob_b, glob_bi);
	printf("\n");
}